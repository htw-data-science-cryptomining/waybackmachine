package main

import (
	s "strings"
	"fmt"
	"bufio"
	"os"
	"net/http"
	"io/ioutil"
	"encoding/csv"
)

var concurrency int = 8
var fileToRead = "majestic_million.csv"
var fileToWrite string = "waybackmachinedata.csv"
var archiveUrl string = "http://web.archive.org/cdx/search/cdx?url="
var archiveFilter string = "&from=2017&to=2017&collapse=timestamp:6&filter=statuscode:200"
var webArchiveUrl string = "https://web.archive.org/web/"

var errCnt = 0
var noDataCnt = 0
var doneCnt = 0
var recCnt = 0

func main() {
	
	workQueue := make(chan string) // This channel has no buffer, so it only accepts input when something is ready
	complete := make(chan bool) // We need to know when everyone is done so we can exit.

	// Read the lines into the work queue.
	go func() {

		file, err := os.Open(fileToRead)
		
		if err != nil {
    		fmt.Println(err)
    	} else {
			defer file.Close() // Close when the functin returns

			scanner := bufio.NewScanner(file)

			for scanner.Scan() {
				workQueue <- scanner.Text()
			}

			close(workQueue) // Close the channel so everyone reading from it knows we're done.	
		}	
	}()

	// Now read them all off, concurrently.
	for i := 0; i < concurrency; i++ {
		go readQueue(workQueue, complete)
	}

	// Wait for everyone to finish.
	for i := 0; i < concurrency; i++ {
		<-complete
	}

	fmt.Println("Done: ", doneCnt)
	fmt.Println("NoData: ", noDataCnt)
	fmt.Println("Errors: ", errCnt)
}

func readQueue(queue chan string, complete chan bool) {
	
	for line := range queue {
		
		var items = s.Split(line, ",") // split line to items

		if len(items) >= 2 {

			var currentDomain string = items[2] // get domain
			var dataByDomain string = getDataFromWbmByDomain(currentDomain) // get data from waybackmachine

			if len(dataByDomain) > 0 {
				var urlCodes []string = getUrlCode(dataByDomain) // cut urlCodes from records

				if len(urlCodes) > 0 {
					var dataToWrite [][]string = createDataString(currentDomain, urlCodes) // daten als string erstellen
					addDataToFile(dataToWrite) // write file	
					doneCnt++;
				}
			} else {
				noDataCnt++;
			}			
		}
	}

	complete <- true // Let the main process know we're done.
}

func getDataFromWbmByDomain(domain string) string {

	var bodyAsString = ""
	var uri = archiveUrl + domain + archiveFilter

	response, err := http.Get(uri) // http get request to waybackmachine
	
	if err != nil {
    	fmt.Println("SERVER ERROR: ", err)
    	errCnt++;
    } else {

		defer response.Body.Close() // close response body
		
		body, err := ioutil.ReadAll(response.Body) // save data	

		if err != nil {
    		fmt.Println(err)
    	} else {
			bodyAsString = string(body) // convert byte to string

			// check if body contains RuntimeErrorException
			if s.Contains(bodyAsString, "RuntimeIOException") {
				bodyAsString = ""
			}

			debugData(domain, len(bodyAsString), uri)
		}
	}

    return bodyAsString // return data as string

}

func getUrlCode(data string) []string {

	var lines = s.Split(data, "\n") // split string to lines
	var urlCodes []string

	for i := 0; i < len(lines); i++ {

		var line = s.Split(lines[i], " ")

		if len(line) > 1 {
			urlCodes = append(urlCodes, line[1])
		}		
	} 
	
	return urlCodes
}

func createDataString(domain string, urlCodes []string) [][]string {

	var data [][]string

	for _, value := range urlCodes {

		var date = value
		if len(date) >= 8 {
			date = value[0:8]
		}
		data = append(data, []string{domain +"," + date + "," + webArchiveUrl + value + "/" + domain})
	}

	return data
}

func addDataToFile(data [][]string) {

	if _, err := os.Stat(fileToWrite); os.IsNotExist(err) {

		file, err := os.Create(fileToWrite) 
		if err != nil {
    		fmt.Println(err)
    	} else {
			defer file.Close()  
		}	
	} 


	file, err := os.OpenFile(fileToWrite, os.O_RDWR|os.O_APPEND, 0777);

	if err != nil {
    	fmt.Println(err)
    } else {
		defer file.Close() // Close when the functin returns

		writer := csv.NewWriter(file)
    	defer writer.Flush()

    	for _, value := range data {
    		err := writer.Write(value)
        	if err != nil {
    			fmt.Println(err)
    		}
    	}
    }
}

func debugData(domain string, bodylength int, uri string) {

	if bodylength > 0  {
		recCnt++
		fmt.Println(recCnt, ": DONE:", domain) // just for output 
	} else {
		recCnt++
    	fmt.Println(recCnt, ": FAIL:", uri) // just for output 
	}
}



